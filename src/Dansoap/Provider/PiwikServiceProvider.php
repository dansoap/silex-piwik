<?php

namespace Dansoap\Provider;

use Dansoap\Twig\Extension\PiwikExtension;
use Silex\Application;
use Silex\ServiceProviderInterface;

class PiwikServiceProvider implements ServiceProviderInterface
{

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     */
    public function boot(Application $app)
    {
    }


    public function register(Application $app)
    {

        if (!isset($app['piwik.options'])) {
            throw new \InvalidArgumentException('Piwik options are not set, you need toset them before registering the service provider');
        }

        $app['piwik.options'] = array_replace(
            array(
                'url' => '',
                'site_id' => 1,
                'enabled' => true
            ),
            $app['piwik.options']
        );

        $app['twig'] = $app->share(
            $app->extend(
                'twig',
                function ($twig, $app) {
                    $twig->addExtension(new PiwikExtension($app['piwik.options']));
                    return $twig;
                }
            )
        );

    }

} 