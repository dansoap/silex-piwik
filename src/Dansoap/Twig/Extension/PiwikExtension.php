<?php

namespace Dansoap\Twig\Extension;


class PiwikExtension extends \Twig_Extension
{

    protected $options;

    protected static $trackingCodeTemplate = <<<ENDPIWIK
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://%PIWIK_URL%/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', %SITE_ID%]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="http://%PIWIK_URL%/piwik.php?idsite=%SITE_ID%" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
ENDPIWIK;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function getName()
    {
        return 'piwik';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('piwik', array($this, 'getPiwikTrackingCode'))
        );
    }

    public function getPiwikTrackingCode()
    {

        if ($this->options['enabled'] !== true) {
            return '';
        }

        $replacements = array(
            '%PIWIK_URL%' => $this->options['url'],
            '%SITE_ID%' => $this->options['site_id']
        );

        return str_replace(array_keys($replacements), $replacements, self::$trackingCodeTemplate);
    }

} 